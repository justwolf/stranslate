# stranslate

## Description

**stranslate** is a simple shell script that allows you to translate text from your terminal, using a [privacy-friendly backend](https://git.sr.ht/~metalune/simplytranslate_web).

**stranslate** uses [SimplyTranslate](https://translate.metalune.xyz) as a backend, which is an ethical translation service that doesn't require JavaScript.
SimplyTranslate can use both Google Translate and LibreTranslate, without compromising your privacy.

## Installation

To install **stranslate** simply run as root:

	make install

## Usage

For detailed information about the usage of the program, see the manual.
